type QuestionResultType = {
  category: string;
  type: string;
  difficulty: string;
  question: string;
  correct_answer: string;
  incorrect_answers: Array<string>;
};

export type QuestionsReturnType = {
  response_code: number;
  results: Array<QuestionResultType>;
};

export const QuestionsNoDataResponse = {
  response_code: -1,
  results: [
    {
      category: "",
      type: "",
      difficulty: "",
      question: "",
      correct_answer: "",
      incorrect_answers: [""],
    },
  ],
};

type CategoryType = {
  id: string;
  name: string;
};

export type CategoryReturnType = {
  trivia_categories: Array<CategoryType>;
};

export const CategoryNoDataResponse = {
  trivia_categories: [{ id: "-1", name: "" }],
};

type CategoryAmountType = {
  total_question_count: number;
  total_easy_question_count: number;
  total_medium_question_count: number;
  total_hard_question_count: number;
};

export type CategoryAmountReturnType = {
  category_id: number;
  category_question_count: CategoryAmountType;
};

export const QuestionNoDataResponse = {
  category_id: -1,
  category_question_count: {
    total_question_count: 0,
    total_easy_question_count: 0,
    total_medium_question_count: 0,
    total_hard_question_count: 0,
  },
};
