import { difficulties } from "@/utils/enums";
import {
  CategoryAmountReturnType,
  QuestionsNoDataResponse,
  CategoryReturnType,
  CategoryNoDataResponse,
  QuestionsReturnType,
  QuestionNoDataResponse,
} from "./types";

const opentdbApiTrivia = "https://opentdb.com/api.php?";
const opentdbAPICategories = "https://opentdb.com/api_category.php";
const opendbAPICategoryCount = "https://opentdb.com/api_count.php?";

/**
 * fetches questions from opentdb
 * @param numQuestions
 * @param categoryId
 * @param difficultyId
 */
export async function fetchQuestions(
  numQuestions = 10,
  categoryId: number,
  difficultyId: number
) {
  try {
    let url = opentdbApiTrivia;

    url += `amount=${numQuestions}`;
    if (categoryId != 0) {
      url += `&category=${categoryId}`;
    }
    if (difficultyId !== 0) {
      const difficulty = difficulties[difficultyId].name.toLowerCase();
      url += `&difficulty=${difficulty}`;
    }

    return await fetch(url).then((r) => r.json());
  } catch (e: any) {
    console.log(e.message);
  }

  return QuestionsNoDataResponse;
}

/**
 * Fetch available categories
 */
export async function fetchCategories() {
  try {
    return await fetch(opentdbAPICategories).then((r) => r.json());
  } catch (e: any) {
    console.log(e.message);
  }
  return CategoryNoDataResponse;
}

/**
 * fetch the number of questions in a given category.
 * Example output:
 * {
 *    "category_id":10,
 *    "category_question_count":
 *    {
 *        "total_question_count":96,
 *        "total_easy_question_count":30,
 *        "total_medium_question_count":40,
 *        "total_hard_question_count":26
 *    }
 *}
 * @param categoryId
 */
export async function fetchQuestionsInCategory(
  categoryId: number
): Promise<CategoryAmountReturnType> {
  try {
    return await fetch(opendbAPICategoryCount + `category=${categoryId}`).then(
      (r) => r.json()
    );
  } catch (e: any) {
    console.log(e.message);
  }

  return QuestionNoDataResponse;
}
