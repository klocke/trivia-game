const triviaAPI = "https://experis-fall2021-assignments.herokuapp.com";
const apiKey = "experis-assignment-api-key";

/*
  More or less copy-pasta from https://github.com/dewald-els/noroff-assignment-api/blob/master/docs/trivia.md
 */

/**
 * Fetch all users matching the specified username.
 * @param username
 */
export async function fetchUsers(username: string) {
  try {
    const results = await fetch(
      `${triviaAPI}/trivia?username=${username}`
    ).then((response) => response.json());
    return [null, results];
  } catch (e: any) {
    console.log(e.message);
    return [e.message, []];
  }
}

/**
 * Create user
 * @param username
 * @param highscore
 */
export async function createUser(username: string, highscore: number) {
  try {
    return await fetch(`${triviaAPI}/trivia`, {
      method: "POST",
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        highScore: highscore,
      }),
    }).then((response) => {
      if (!response.ok) {
        throw new Error("Could not create new user");
      }
      return response.json();
    });
  } catch (e: any) {
    console.log(e.message);
  }
}

/**
 * Update high score for user
 * @param userId
 * @param highscore
 */
export async function updateHighscore(userId: number, highscore: number) {
  try {
    return await fetch(`${triviaAPI}/trivia/${userId}`, {
      method: "PATCH", // NB: Set method to PATCH
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        highScore: highscore,
      }),
    }).then((response) => {
      if (!response.ok) {
        throw new Error("Could not update high score");
      }
      return response.json();
    });
  } catch (e: any) {
    console.log(e.message);
  }
}
