import {
  fetchQuestions,
  fetchQuestionsInCategory,
} from "@/apiHelpers/opentdbAPI";
import { createUser, fetchUsers } from "@/apiHelpers/triviaAPI";
import router from "@/router";
import { State, Question } from "@/store/state";
import { Commit } from "vuex";
import { shuffle } from "./utils";

/**
 * Fetches the maximum amount of questions given category and difficultyId
 * @param categoryId
 * @param difficultyId
 */
export async function maxNumQuestion(
  categoryId: number,
  difficultyId: number
): Promise<number> {
  let num: number;

  if (Number(categoryId) === 0) {
    return 50;
  }
  const { category_question_count } = await fetchQuestionsInCategory(
    categoryId
  );

  difficultyId = Number(difficultyId);
  if (difficultyId === 0) {
    num = category_question_count.total_question_count;
  } else if (difficultyId === 1) {
    num = category_question_count.total_easy_question_count;
  } else if (difficultyId === 2) {
    num = category_question_count.total_medium_question_count;
  } else if (difficultyId === 3) {
    num = category_question_count.total_hard_question_count;
  } else {
    throw new Error("Unrecognized difficultyId");
  }

  return num;
}

/**
 * https://stackoverflow.com/q/679915/10074443
 *
 * Efficient way to determine if an object is empty.
 * @param obj
 */
export function isEmpty(obj: any): boolean {
  for (const i in obj) {
    return false;
  }
  return true;
}

/**
 * Helper function for starting a new trivia.
 *
 * Fetches user, resets stats, and routes to questions page.
 *
 */
export async function startTriviaHandler(
  state: State,
  commit: Commit
): Promise<void> {
  let user = (await fetchUsers(state.username))[1][0];
  if (isEmpty(user)) {
    console.log("User not in api, creating new.");
    user = await createUser(state.username, 0);
  }
  commit("setUserId", user.id);
  commit("setUserScore", 0);
  commit("setHighscore", user.highScore);
  commit("resetAnswers");
  commit("resetCurrentQuestionId");
  const { results: questions } = await fetchQuestions(
    state.questionAmount,
    state.chosenCategory,
    state.chosenDifficulty
  );

  const formattedQuestions: Question[] = [];
  const questionAnswers: Array<string> = [];

  for (const question of questions) {
    formattedQuestions.push({
      question: question.question,
      answerSuggestions: shuffle([
        ...question.incorrect_answers,
        question.correct_answer,
      ]),
    });
    questionAnswers.push(question.correct_answer);
  }

  commit("setQuestions", formattedQuestions);
  commit("setCorrectAnswers", questionAnswers);
  await router.push({ name: "Question" });
}
