export const difficulties = [
  {
    id: 0,
    name: "Any",
  },
  {
    id: 1,
    name: "Easy",
  },
  {
    id: 2,
    name: "Medium",
  },
  {
    id: 3,
    name: "Hard",
  },
];
