import { Commit } from "vuex";
import { State } from "./state";
import { fetchCategories } from "@/apiHelpers/opentdbAPI";
import { maxNumQuestion, startTriviaHandler } from "@/utils/handlers";
import router from "@/router";
import { updateHighscore } from "@/apiHelpers/triviaAPI";

export const fetchCategoriesFromAPI = async ({
  commit,
}: {
  commit: Commit;
}): Promise<void> => {
  let { trivia_categories } = await fetchCategories();
  trivia_categories = [{ id: "0", name: "Any" }, ...trivia_categories];
  commit("setCategories", trivia_categories);
};

export const onCategoryChangeHandler = (
  { commit }: { commit: Commit },
  { target: { value: categoryId } }: { target: HTMLInputElement; value: number }
): void => {
  commit("setChosenCategory", categoryId);
};

export const onDifficultyChangeHandler = (
  { commit }: { commit: Commit },
  {
    target: { value: difficultyId },
  }: { target: HTMLInputElement; value: number }
): void => {
  commit("setDifficulty", difficultyId);
};

export const onQuestionAmountChangeHandler = (
  { commit }: { commit: Commit },
  questionAmount: number
): void => {
  commit("setQuestionAmount", questionAmount);
};

export const onNumberInputChangeHandler = (
  { commit }: { commit: Commit },
  {
    target: { value: numberOfQuestions },
  }: { target: HTMLInputElement; value: number }
): void => {
  commit("setQuestionAmount", Math.min(50, Number(numberOfQuestions)));
};

/**
 * Action that triggers when a new trivia should be started.
 * @param commit
 * @param state
 */
export const onClickStartHandler = async ({
  commit,
  state,
}: {
  commit: Commit;
  state: State;
}): Promise<void> => {
  if (state.username.trim() === "") {
    alert("Username cannot be empty");
    return;
  }

  if (state.isLoading) {
    return;
  } else {
    commit("setIsLoading", true);
  }

  const maxNum = await maxNumQuestion(
    state.chosenCategory,
    state.chosenDifficulty
  );

  if (maxNum < state.questionAmount) {
    alert(
      `Not enough existing questions in api for given category and 
          difficulty, setting the amount of questions to ${maxNum}.`
    );
    commit("setQuestionAmount", maxNum);
  }
  commit("setAvailableQuestions", Math.min(50, maxNum));
  await startTriviaHandler(state, commit);
  commit("setIsLoading", false);
};

export const onTextInputHandler = (
  {
    commit,
  }: {
    commit: Commit;
  },
  { target: { value: userInput } }: { target: HTMLInputElement; value: string }
): void => {
  commit("setUsername", userInput);
};

/**
 * Function to calculate the trivia score before result screen
 * @param commit
 * @param state
 */
export const calculateScore = async ({
  commit,
  state,
}: {
  commit: Commit;
  state: State;
}): Promise<void> => {
  let score = 0;
  for (let i = 0; i < state.userAnswers.length; i++) {
    if (state.userAnswers[i] === state.correctAnswers[i]) {
      score += 10;
    }
  }

  if (score > state.highscore) {
    const res = await updateHighscore(state.userId, score);
    commit("setHighscore", res.highScore);
  }

  commit("setUserScore", score);
};

/**
 * Action triggered when next question is clicked
 * @param commit
 * @param state
 */
export const nextQuestion = async ({
  commit,
  state,
}: {
  commit: Commit;
  state: State;
}): Promise<void> => {
  if (state.currentChosenAnswer === "") {
    alert("You need to choose an answer");
    return;
  }

  commit("appendUserAnswer", state.currentChosenAnswer);
  if (state.currentQuestionId === state.questions.length - 1) {
    await router.push({ name: "Result" });
  } else {
    commit("nextQuestion");
    commit("setCurrentChosenAnswer", "");
  }
};

/**
 * Register answer by user.
 * @param commit
 * @param userInput
 */
export const onClickSuggestedAnswerAction = (
  { commit }: { commit: Commit },
  { target: { value: userInput } }: { target: HTMLInputElement; value: string }
): void => {
  commit("setCurrentChosenAnswer", userInput);
};

/**
 * Action triggered by "try-again" button
 * @param commit
 * @param state
 */
export const onTryAgainHandler = async ({
  commit,
  state,
}: {
  commit: Commit;
  state: State;
}): Promise<void> => {
  commit("setIsLoading", true);
  await startTriviaHandler(state, commit);
  commit("setIsLoading", false);
};

/**
 * Action triggered by "new-game" button.
 * @param commit
 */
export const onNewGameHandler = async ({
  commit,
}: {
  commit: Commit;
}): Promise<void> => {
  commit("resetEverything");
  await router.push({ name: "Home" });
};
