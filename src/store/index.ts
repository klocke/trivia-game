import { createStore } from "vuex";

import * as actions from "./actions";
import mutations from "./mutations";
import { state } from "@/store/state";
import modules from "@/store/modules";
import getters from "@/store/getters";

export default createStore({
  state,
  mutations,
  actions,
  modules,
  getters,
});
