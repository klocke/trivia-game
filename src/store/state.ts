import { difficulties } from "@/utils/enums";

export type Categories = {
  id: number;
  name: string;
};

export type Difficulties = {
  id: number;
  name: string;
};

export type Question = {
  question: string;
  answerSuggestions: Array<string>;
};

export type State = {
  isLoading: boolean;
  userId: number;
  username: string;
  userScore: number;
  highscore: number;
  questionAmount: number;
  availableQuestions: number;
  categories: Array<Categories>;
  chosenCategory: number;
  difficulties: Array<Difficulties>;
  chosenDifficulty: number;
  questions: Array<Question>;
  userAnswers: Array<string>;
  correctAnswers: Array<string>;
  currentQuestionId: number;
  currentChosenAnswer: string;
};

export const state: State = {
  isLoading: false,
  userId: -1,
  username: "",
  userScore: 0,
  highscore: 0,
  questionAmount: 10,
  availableQuestions: 50,
  categories: [],
  chosenCategory: 0,
  difficulties: difficulties,
  chosenDifficulty: 0,
  questions: [],
  currentQuestionId: 0,
  userAnswers: [],
  correctAnswers: [],
  currentChosenAnswer: "",
};
