import { State } from "./state";

export default {
  question: (state: State): string => {
    try {
      const q = state.questions[state.currentQuestionId];
      return q.question;
    } catch (e) {
      return "";
    }
  },
  answerSuggestions: (state: State): Array<string> => {
    try {
      const q = state.questions[state.currentQuestionId];
      return q.answerSuggestions;
    } catch (e) {
      return [];
    }
  },
  currentQuestions: (state: State): number => {
    return state.currentQuestionId + 1;
  },
  TotalQuestions: (state: State): number => {
    return state.questions.length;
  },
};
