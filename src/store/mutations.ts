import { State, Categories, Question } from "./state";

export default {
  setIsLoading: (state: State, payload: boolean): void => {
    state.isLoading = payload;
  },
  setUsername: (state: State, payload: string): void => {
    state.username = payload;
  },
  setUserScore: (state: State, payload: number): void => {
    state.userScore = payload;
  },
  setHighscore: (state: State, payload: number): void => {
    state.highscore = payload;
  },
  setQuestionAmount: (state: State, payload: number): void => {
    state.questionAmount = payload;
  },
  setCategories: (state: State, payload: Array<Categories>): void => {
    state.categories = payload;
  },
  setChosenCategory: (state: State, payload: number): void => {
    state.chosenCategory = payload;
  },
  setDifficulty: (state: State, payload: number): void => {
    state.chosenDifficulty = payload;
  },
  setQuestions: (state: State, payload: Array<Question>): void => {
    state.questions = payload;
  },
  setAvailableQuestions: (state: State, payload: number): void => {
    state.availableQuestions = payload;
  },
  nextQuestion: (state: State): void => {
    state.currentQuestionId++;
  },
  setCurrentChosenAnswer: (state: State, payload: string): void => {
    state.currentChosenAnswer = payload;
  },
  appendUserAnswer: (state: State, payload: string): void => {
    state.userAnswers.push(payload);
  },
  setCorrectAnswers: (state: State, payload: Array<string>): void => {
    state.correctAnswers = payload;
  },
  resetAnswers: (state: State): void => {
    state.userAnswers = [];
    state.correctAnswers = [];
  },
  setUserId: (state: State, payload: number): void => {
    state.userId = payload;
  },
  resetCurrentQuestionId: (state: State): void => {
    state.currentQuestionId = 0;
  },
  resetEverything: (state: State): void => {
    state.userId = -1;
    state.username = "";
    state.userScore = 0;
    state.highscore = 0;
    state.questionAmount = 10;
    state.availableQuestions = 50;
    state.categories = [];
    state.chosenCategory = 0;
    state.chosenDifficulty = 0;
    state.questions = [];
    state.currentQuestionId = 0;
    state.userAnswers = [];
    state.correctAnswers = [];
    state.currentChosenAnswer = "";
  },
};
